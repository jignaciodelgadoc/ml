#!/usr/bin/env python3

from config import config
from imutils import paths
from scipy import misc
from h5.h5DatasetWriter import H5DatasetWriter 
import shutil
import random
import cv2 as cv
import os

# Creating the output directories if they do not exist
for outputPath in [config.IMAGES_PATH, config.LABELS_PATH]:
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)

# Getting image paths
print("[INFO:] Creating temporary images...")
imagePaths = list(paths.list_images(config.INPUT_IMAGES_PATH))
random.shuffle(imagePaths)
totalImagesProcessed = 0

numImages = 0
# Looping over the list of image paths
for imagePath in imagePaths:
    # Reading image
    numImages = numImages + 1
    print("Num. Images: ", numImages)
    image = cv.imread(imagePath)
    # Obtaining dimensions from the image and cropping it so the subimages can be extracted nicely
    (height, width) = image.shape[:2]
    width -= int(width % config.SCALE)
    height -= int(height % config.SCALE)
    image = image[0 : height, 0 : width]

    # Downscaling the image by the scaler factor and then upscaling it for the same factor but using bicubic interpolation
    scaledImage = cv.resize(image, (int(width/config.SCALE), int(height/config.SCALE)), interpolation=cv.INTER_CUBIC)
    lrImage = cv.resize(scaledImage, (width,height), interpolation=cv.INTER_CUBIC)

    # Extracting subimage
    for verticalIterator in range(0, height - config.INPUT_SIZE + 1, config.STRIDE):
        for horizontalIterator in range(0, width - config.INPUT_SIZE + 1, config.STRIDE):
            crop = lrImage[verticalIterator : verticalIterator + config.INPUT_SIZE, horizontalIterator : horizontalIterator + config.INPUT_SIZE]
            
            target = image[verticalIterator +   config.PAD : verticalIterator +   config.PAD + config.LABEL_SIZE,
                           horizontalIterator + config.PAD : horizontalIterator + config.PAD + config.LABEL_SIZE]
            
            # Path to output crop images
            cropPath = os.path.sep.join([config.IMAGES_PATH, "{}.png".format(totalImagesProcessed)])
            targetPath = os.path.sep.join([config.LABELS_PATH, "{}.png".format(totalImagesProcessed)])
            
            # Saving images
            cv.imwrite(cropPath, crop)
            cv.imwrite(targetPath, target)

            totalImagesProcessed += 1
# Path to images
print("[INFO:] Building HDF5 datasets...")
inputPaths = sorted(list(paths.list_images(config.IMAGES_PATH)))
outputPaths = sorted(list(paths.list_images(config.LABELS_PATH)))

# Initializing dataset
inputWriter = H5DatasetWriter((len(inputPaths), config.INPUT_SIZE, config.INPUT_SIZE, 3), config.IMAGES_H5_PATH)
outputWriter = H5DatasetWriter((len(outputPaths), config.LABEL_SIZE, config.LABEL_SIZE, 3), config.LABELS_H5_PATH)

# Looping over the image
for (inputPath, outputPath) in zip(inputPaths, outputPaths):
    inputImage = cv.imread(inputPath)
    outputImage = cv.imread(outputPath)
    inputWriter.add([inputImage], [-1])
    outputWriter.add([outputImage],[-1])

# Close H5DF datasets
inputWriter.close()
outputWriter.close()

# Deleting temporary output directories
print("[INFO:] Cleaning up ...")
shutil.rmtree(config.IMAGES_PATH)
shutil.rmtree(config.LABELS_PATH)
