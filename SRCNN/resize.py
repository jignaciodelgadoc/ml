#!/usr/bin/env python3

from config import config
from scipy import misc
from srcnn.srcnn import SRCNN
import numpy as np
import os
import argparse
import cv2 as cv
import tensorflow as tf
import tensorflow.keras.models

#physical_devices = tf.config.experimental.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)

# Constructing argument parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "path to input image")
ap.add_argument("-b", "--baseline", required = True, help = "path to baseline image")
ap.add_argument("-o", "--output", required = True, help = "path to output image")
args = vars(ap.parse_args())

# Loading model
print ("[INFO] Loading model...")
#model = tensorflow.keras.models.load_model(config.MODEL_PATH)
checkpoint_path = "training_1/cp-0015.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
model = SRCNN.build(width = config.INPUT_SIZE, height = config.INPUT_SIZE, depth = 3)
#latest = tf.train.latest_checkpoint(checkpoint_dir)
model.load_weights(checkpoint_path)
model.summary()


# Loading imput image, obtaining the dims and cropping for nice subimage extraction 
print("[INFO] Generating image...")
image = cv.imread(args["image"])
(height, width) = image.shape[:2]
width -= int(width % config.SCALE)
height -= int(height % config.SCALE)
image = image[0 : height, 0 : width]

# Resizing input image using bicubin interpolation and saving it
scaled = cv.resize(image, (int(width*config.SCALE) ,int(height * config.SCALE)), interpolation=cv.INTER_CUBIC)
#scaled = cv.resize(scaled, (int(width) ,int(height)), interpolation=cv.INTER_CUBIC)
cv.imwrite(args["baseline"],scaled)

# Allocating memory for the output image
output = np.zeros(scaled.shape)
(height, width) = output.shape[:2]

# Sliding a window
for verticalIt in range(0, height - config.INPUT_SIZE + 1, config.LABEL_SIZE):
    for horizontalIt in range(0, width - config.INPUT_SIZE + 1, config.LABEL_SIZE):
        # Cropping ROI from scaled image
        crop = scaled[verticalIt : verticalIt + config.INPUT_SIZE, horizontalIt : horizontalIt + config.INPUT_SIZE]

        # Making a prediction over the crop
        prediction = model.predict(np.expand_dims(crop, axis = 0))
        prediction = prediction.reshape((config.LABEL_SIZE, config.LABEL_SIZE, 3))
        output[verticalIt + config.PAD : verticalIt + config.PAD + config.LABEL_SIZE,
               horizontalIt + config.PAD : horizontalIt + config.PAD + config.LABEL_SIZE] = prediction

# Removing black borders and any values out of [0, 255]
output = output[config.PAD : height - ((height % config.INPUT_SIZE) + config.PAD),
                config.PAD : width - ((width % config.INPUT_SIZE) + config.PAD)]
output = np.clip(output, 0 , 255).astype("uint8")

# Write the output image to disk
cv.imwrite(args["output"], output)

#image = scaled[config.PAD : height - ((height % config.INPUT_SIZE) + config.PAD),
                #config.PAD : width - ((width % config.INPUT_SIZE) + config.PAD)]

#scaled = scaled[config.PAD : height - ((height % config.INPUT_SIZE) + config.PAD),
                #config.PAD : width - ((width % config.INPUT_SIZE) + config.PAD)]
# Estimating PSNR
#print("PSNR Bicubic: ", image.shape, ",  ", scaled.shape)
#print (cv.PSNR(image, scaled))
#print("PSNR SR: ", image.shape, ",  ", output.shape)
#print (cv.PSNR(image, output))