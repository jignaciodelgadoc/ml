#!/usr/bin/env python3

import os

# Path to image dataset
INPUT_IMAGES_PATH = "input/images"

# Path to temporary output directories
BASE_OUTPUT_PATH = "output"
IMAGES_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "images"])
LABELS_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "labels"])

# Path to HDF5 files
IMAGES_H5_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "images.hdf5"])
LABELS_H5_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "labels.hdf5"])

# Path to the output model and plot file 
MODEL_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "srcnn.model"])
PLOT_PATH = os.path.sep.join([BASE_OUTPUT_PATH, "plot.png"])

# Config constants
BATCH_SIZE = 32
NUM_EPOCHS = 15
SCALE = 2.0 #Scaling applied to the image
INPUT_SIZE = 33 # Dimension of the input 
LABEL_SIZE = 21 # Dimension of the output of the SRCNN
PAD = int((INPUT_SIZE - LABEL_SIZE) / 2.0) # Ensures crop the label ROI
STRIDE = 14 # Step size of the sliding window
