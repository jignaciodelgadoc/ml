#!/usr/bin/env python3

import tensorflow as tf
import tensorflow.keras.utils
import numpy as np
import h5py

class H5DatasetReader:

    def __init__(self, dbPath, batchSize, binarize = True, classes = 2):

        # Storing values 
        self.batchSize = batchSize
        self.binarize = binarize
        self.classes = classes

        # Opening hdf5 dataset and obtaining size
        self.db = h5py.File(dbPath)
        self.numImages = self.db["labels"].shape[0]

    # Closing the dataset
    def close(self):

        self.db.close()

    # Generating image and label batches from dataset
    def generator(self, passes = np.inf):

        epochs = 0

        # Looping infinitely. The model will stop when reached the desired number of epochs
        while epochs < passes:
            #Looping through the dataset with steps of size batchSize
            for index in np.arange(0, self.numImages, self.batchSize):
                images = self.db["images"][index : index + self.batchSize]
                labels = self.db["labels"][index : index + self.batchSize]
                
                if self.binarize:
                    labels = tensorflow.keras.utils.to_categorical(labels, self.classes) # Converts a class vector (integers) into a binary class matrix [0, 1, 2] -> [1, 0, 0][0, 1, 0][0, 0, 1]

                # Yielding a batch of images and labels
                yield (images, labels) # Returns the batches and resume the execution. Used to avoid storing in memory all the batches together
            
            epochs += 1