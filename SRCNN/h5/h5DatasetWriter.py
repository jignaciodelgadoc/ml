#!/usr/bin/env python3

import h5py
import os

class H5DatasetWriter:

    def __init__(self, dims, outputPath, dataKey = "images", buffSize = 1000):

        # Checking if the output path exists
        #if os.path.exists(outputPath):
        #    raise ValueError("The supplied 'outputPath' already exists and cannot be overwritten. Manually delete the file before continuing", outputPath)

        # Opening the database files in writing mode and creating two datasets (images and labels)
        self.db = h5py.File(outputPath, "w")
        self.data = self.db.create_dataset(dataKey, dims, dtype = "float")
        self.labels = self.db.create_dataset("labels", (dims[0],), dtype = "int")

        # Storing parameters
        self.buffSize = buffSize
        self.buffer = {"data" : [], "labels" : []}
        self.idx = 0
    
    # Adding the rows and labels to the buffer
    def add(self, rows, labels):
        
        self.buffer["data"].extend(rows)
        self.buffer["labels"].extend(labels)

        # Checking if the buffer needs to be flushed to the disk
        if len(self.buffer["data"]) >= self.buffSize:
            self.flush()
    
    # Checking if there is data in the buffer that needed to be flushed into the disk
    def close(self):
        
        if len(self.buffer["data"]) > 0:
            self.flush()

        # Closing the dataset    
        self.db.close()

    # Writing the buffers to the disk and reseting them
    def flush(self):
        
        maxIndex = self.idx + len(self.buffer["data"])
        self.data[self.idx : maxIndex] = self.buffer["data"]
        self.labels[self.idx : maxIndex] = self.buffer["labels"]
        self.idx = maxIndex
        self.buffer = {"data" : [], "labels" : []}

    # Creating a dataset to store the actual class label names and then, storing the class labels
    def storeClassLabels(self, classLabels):
        
        dt = h5py.special_dtype(vlen = str)
        labelSet = self.db.create_dataset("label_names", (len(classLabels),), dtype = dt)
        labelSet[:] = classLabels
