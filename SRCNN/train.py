#!/usr/bin/env python3

import matplotlib
matplotlib.use("Agg")

from config import config
from h5.h5DatasetReader import H5DatasetReader
from srcnn.srcnn import SRCNN

import tensorflow as tf
from tensorflow.keras import datasets, layers, models, backend, utils, losses

import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
import os

#physical_devices = tf.config.experimental.list_physical_devices('GPU')
#tf.config.experimental.set_memory_growth(physical_devices[0], True)


def sr_generator(inputDataGen, targetDataGen):

    while True:
        # Grabbing the data and label
        inputData = next(inputDataGen)[0]
        targetData = next(targetDataGen)[0]

        yield(inputData, targetData)

# Checkpoints function
checkpoint_path = "training_1/cp-{epoch:04d}.ckpt"
cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path, 
    verbose=1, 
    save_weights_only=True,
    period=5)

# Intiliazing the input images and target output images generator
inputs = H5DatasetReader(config.IMAGES_H5_PATH, config.BATCH_SIZE)
targets = H5DatasetReader(config.LABELS_H5_PATH, config.BATCH_SIZE)

# Initializing the model and compiling it
print("[INFO] Compiling model...")
optimizer = tf.keras.optimizers.Adam(learning_rate = 0.0001, decay = 0.0001/config.NUM_EPOCHS)
model = SRCNN.build(width = config.INPUT_SIZE, height = config.INPUT_SIZE, depth = 3)
model.summary()
checkpoint_path = "training_1/cp-0005.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
model.load_weights(checkpoint_path)
model.compile(loss = "mae", optimizer = optimizer)

# Training the model with generators
trainedModel = model.fit(sr_generator(inputs.generator(), targets.generator()),
                    steps_per_epoch = inputs.numImages // config.BATCH_SIZE,
                    epochs = config.NUM_EPOCHS,
                    callbacks=[cp_callback],
                    verbose = 1)

# Saving the model
print("[INFO] Saving the model")
model.save(config.MODEL_PATH, overwrite = True)

# Plotting training loss
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, config.NUM_EPOCHS), trainedModel.history["loss"], label= "loss")
plt.title("Loss on SR training")
plt.xlabel("Epochs #")
plt.ylabel("Loss")
plt.legend()
plt.savefig(config.PLOT_PATH)
