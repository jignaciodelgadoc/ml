#!/usr/bin/env python3

import tensorflow as tf

from tensorflow.keras import datasets, layers, models, backend, initializers, utils

class SRCNN:
    
    @staticmethod
    def build(width, height, depth):
        
        model = models.Sequential()
        inputShape = (height, width, depth)

        # SRCNN
        model.add(layers.Conv2D(64, (9, 9), activation='relu', input_shape = inputShape, kernel_initializer='he_normal'))
        model.add(layers.Conv2D(32, (1, 1), activation='relu', kernel_initializer='he_normal'))
        model.add(layers.Conv2D(depth, (5, 5), activation='relu', kernel_initializer='he_normal'))
        return model
        
