#!/usr/bin/env python3

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

import numpy as np
import matplotlib.pyplot as plt
import scipy.misc

gen = tf.keras.preprocessing.image.ImageDataGenerator(rotation_range=10, width_shift_range=0.1,
                                                      height_shift_range=0.1, shear_range=0.15, 
                                                      zoom_range=0.1, channel_shift_range=10,
                                                      horizontal_flip=True)
imagePath = 'golden.jpg'

image = np.expand_dims(plt.imread(imagePath),0)
plt.imshow(image[0])
#plt.show()

augmentedIter = gen.flow(image)
augmentedImages = [next(augmentedIter)[0].astype(np.uint8) for i in range(10)]

plt.imshow(augmentedImages[2])
plt.show()